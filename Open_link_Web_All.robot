*** Settings ***
Library           Selenium2Library
*** Variables ***
${Browser_headless}             headlesschrome
${Browser}                      headlesschrome       #chrome
${Web_advice}                   https://www.advice.co.th/ 
${Web_work_advice}              https://work.advice.co.th
${Web_headdaddy}                https://www.headdaddy.com
${Web_branch_advice}            https://branch.advice.co.th/
${Web_Work_Branch}              https://work.advice.co.th/branch
${Web_Shop}                     http://www.advicect.com
${Web_Mini}                     http://www.bbthoeng.com
${cuscodelogin}                 80006225
${passwordcuscode}              uat1234
${cuscodelogin2}                Uathdd4@gmail.com
${passwordcuscode2}             uat1234
${personellogin}                555555
${passwordpersonel}             Adv@Pass01
${personellogin2}               000140
${passwordpersonel2}            data1602
${ADD_User}                     threerapong.advice@gmail.com    #threerapong.advice@gmail.com
${ADD_password}                 1234 



*** Test Cases ***
Web Page Web.advice.co.th dealer
    Open Browser          ${Web_advice}          ${Browser}
    Maximize Browser Window
    Go to                       https://www.advice.co.th/advicedealer/login
    Set Browser Implicit Wait               1
    Input Text                              id=login-user               ${cuscodelogin}
    Input Text                              id=login-pass               ${passwordcuscode}
    Click Element                           //*[@id="btn-login"]
    Close Browser

Web Page Web.advice.co.th adviceonline
   open browser          ${Web_advice}          ${Browser}
   Set Window Size          1920         1080          #Maximize Browser Window
   Set Browser Implicit Wait          1
   Go to                       https://www.advice.co.th/adviceonline/login
# #Drop_down_login
#     Mouse Down             //html/body/header[1]/div[2]/div/div/div[2]/div[1]/div[2]/div/ul/li[4]/a
#     Set Browser Implicit Wait          1
# #Click_login
#     Click Element          //html/body/header[1]/div[2]/div/div/div[2]/div[1]/div[2]/div/ul/li[4]/div/ul/li[1]/a
#     Set Browser Implicit Wait          1
#Add Username
    [Arguments]          ${ADD_User}
    Input Text  id=email-signin          ${ADD_User}
    Set Browser Implicit Wait          1
#Add password
    [Arguments]          ${ADD_password}
    Input Text  id=pass-signin          ${ADD_password}
    Set Browser Implicit Wait          1
#Click login Button
    Click Element          //*[@id="signin-form-new"]/div[4]
    Alert Should Not Be Present          timeout=10
#เช็คเข้าสู่ระบบสำเร็จ
    Element Text Should Be         //html/body/header[1]/div[2]/div/div/div[2]/div[1]/div[2]/div/ul/li[4]/a            ข้อมูลส่วนตัว
    Close Browser

Web Page work.advice.co.th
    Open Browser          ${Web_work_advice}          ${Browser}
    Maximize Browser Window
    Set Browser Implicit Wait               1
    Input Text                              id=txt_uname                ${personellogin}
    Input Text                              id=txt_password             ${passwordpersonel}
    Click Element                           //*[@id="loginId"]
    Close Browser

Web Page headdaddy.com
    Open Browser          ${Web_headdaddy}          ${Browser}
    Maximize Browser Window
    Set Browser Implicit Wait               1
    Input Text                              name=user                   ${cuscodelogin2}
    Input Text                              name=pass                   ${passwordcuscode2}
    Click Element                           //*[@id="form_comment"]/div[1]
    Close Browser

Login Web branch.advice.co.th
    Open Browser          ${Web_branch_advice}          ${Browser}
    Maximize Browser Window
    Set Browser Implicit Wait               1
    Click Element                           //*[@id="select-branch"]/div/div[1]
    Click Element                           //*[@id="select-branch"]/div/div[2]/div/div[2]
    Input Text                              id=username                  ${personellogin2}
    Input Text                              id=pass                      ${passwordpersonel2}
    Click Element                           xpath:/html/body/div[1]/div/div[2]/div[2]/div[4]/button
    Close Browser

Web Work Branch
    Open Browser          ${Web_Work_Branch}          ${Browser}
    Maximize Browser Window
    Set Browser Implicit Wait               1
    Click Element                           //*[@id="headingAvi"]/h1/button
    Click Element                           //*[@id="collapseOne0"]/ul/li[3]/a
    Set Browser Implicit Wait               1
    Input Text                              id=cuscode                    ${personellogin2}
    Input Text                              id=password                   ${passwordpersonel2}
    Click Element                           //*[@id="formContent"]/input[3]
    Close Browser

Web Page Shop
    Open Browser          ${Web_Shop}          ${Browser}
    Maximize Browser Window
    Set Browser Implicit Wait               1
    Input Text                              id=cust                        80006225
    Input Text                              id=passs                       uat1234
    Click Element                           //*[@id="Submit_dealer"]
    Close Browser   

Web Page Mini
    Open Browser          ${Web_Mini}          ${Browser}
    Maximize Browser Window
    Set Browser Implicit Wait               1
    Input Text                              id=cust                        ${cuscodelogin}
    Input Text                              id=passs                       ${passwordcuscode}
    Click Element                           //*[@id="Submit_dealer"]
    Close Browser
