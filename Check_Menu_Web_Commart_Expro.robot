*** Settings ***
Library          SeleniumLibrary

*** Variables ***
${Browser}             gc
${Link_Web_itexpro}    https://itexpo.advice.co.th/
${Link_Web_itexpro2}    https://itexpo.advice.co.th/comset
${Product_Code}        A0129984
${product quantity}    1
${User_Login}          80006225
${Pass_Login}          uat1234
${headless_mode}       headlesschrome

*** Test Cases ***
Open_Web_itexpo
    #Open Browser          ${Link_Web_itexpro}          ${headless_mode}
    #Open Browser          ${Link_Web_itexpro}          ${Browser}
    Open Browser          ${Link_Web_itexpro2}          ${Browser}
    Maximize Browser Window
    Set Selenium Implicit Wait          1

test_Menu_Computer Set Promotion
     Element Text Should Be     //html/body/section[2]/div/div/div[2]/div[2]/div/div        Computer Set Promotion

test_Menu_Notebook_Notebook
     Click Element      //*[@id="primary_nav_wrap"]/ul/li[2]/a
     Click Element      //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label        Notebook HUAWEI
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label        Notebook ASUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label       Notebook ASUS PROART
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label        Notebook LENOVO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label        Notebook MSI
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label        Notebook HP
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label        Notebook DELL
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label        Notebook ACER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label        Notebook ACER CONCEPTD

test_Menu_Notebook_Notebook Gaming
     Click Element      //*[@id="primary_nav_wrap"]/ul/li[2]/a
     Click Element      //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label         Notebook Gaming ASUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label         Notebook Gaming LENOVO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label         Notebook Gaming MSI
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label         Notebook Gaming ACER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label         Notebook Gaming HP
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label         Notebook Gaming DELL

test_Menu_Notebook_Notebook 2in1
     Click Element      //*[@id="primary_nav_wrap"]/ul/li[2]/a
     Click Element      //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label         Notebook 2in1 HP
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label         Notebook 2in1 LENOVO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label         Notebook 2in1 ASUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label         Notebook 2in1 DELL

test_Menu_DESKTOP/AIO/SERVER_All-in-one PC
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[3]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label         All-in-One PC ACER 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label         All-in-One PC HP 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label         All-in-One PC LENOVO 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label         All-in-One PC ASUS 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label         All-in-One PC DELL
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label         All-in-One PC MSI

test_Menu_DESKTOP/AIO/SERVER_Desktop PC
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[3]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label         Desktop PC ACER  
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label         Desktop PC LENOVO  
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label         Desktop PC DELL  
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label         Desktop PC HP  
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label         Desktop PC ASUS 

test_Menu_DESKTOP/AIO/SERVER_Mini PC
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[3]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label         Mini PC INTEL

test_Menu_COMPUTER HARDWARE (DIY)_CPU
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          AMD FM2+
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          AMD AM4 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          AMD TR4 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          INTEL 1200 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          INTEL 2066 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          INTEL 1151V2 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          INTEL 1151 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          INTEL 1150 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         INTEL 1155 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         INTEL 775 

test_Menu_COMPUTER HARDWARE (DIY)_Mainboard
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          MAINBOARD + CPU 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          INTEL (775) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          INTEL (1150, 1155, 1156) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          INTEL 1151 (H110, 170) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          INTEL 1151V2 (H310) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          INTEL 1151V2 (B365) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          INTEL 1151V2 (Z390) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          INTEL 1200 (B460) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         INTEL 1200 (H410) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         INTEL 1200 (H470) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         INTEL 1200 (Z490) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         AMD (AM2, AM3+) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[14]/div/a/label         AMD AM4 (A320)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[15]/div/a/label         AMD AM4 (A520)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[16]/div/a/label         AMD AM4 (B450) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[17]/div/a/label         AMD AM4 (B550)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[18]/div/a/label         AMD AM4 (X470) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[19]/div/a/label         AMD AM4 (X570) 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[20]/div/a/label         AMD TR4 (X399)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[21]/div/a/label         AMD sTRX4 (TRX-40)

test_Menu_COMPUTER HARDWARE (DIY)_Graphic Card
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          AMD RADEON R SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          AMD RADEON RX SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          AMD RADEON RX5500XT SERIES 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          AMD RADEON RX5600XT SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          AMD RADEON RX5700 SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          NVIDIA 200 SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          NVIDIA 600 SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          NVIDIA 700 -1030 SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         NVIDIA 1650 SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         NVIDIA 1650 SUPER SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         NVIDIA 1660 - 1660TI SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         NVIDIA 1660 SUPER SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[14]/div/a/label         NVIDIA 2060 SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[15]/div/a/label         NVIDIA 2070 - 2080 SUPER SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[16]/div/a/label         NVIDIA 3090 SERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[17]/div/a/label         NVIDIA QUADRO
     
test_Menu_COMPUTER HARDWARE (DIY)_Ram for PC
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          PC DDR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          PC DDR3(1066)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          PC DDR3(1333)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          PC DDR3(1600)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          PC DDR3L(1600)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          PC DDR4(2133)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          PC DDR4(2400)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          PC DDR4(2666)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         PC DDR4(3000)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         PC DDR4(3200)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         PC DDR4(3466)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         PC DDR4(3600)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[14]/div/a/label         PC DDR4(4000)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[15]/div/a/label         PC DDR4(4133)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[16]/div/a/label         PC DDR4(4400)

test_Menu_COMPUTER HARDWARE (DIY)_Ram for NoteBook
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          NoteBook DDR2
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          NoteBook DDR3(1066)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          NoteBook DDR3(1333)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          NoteBook DDR3(1600)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          NoteBook DDR3L(1600)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          NoteBook DDR4(2133)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          NoteBook DDR4(2400)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          NoteBook DDR4(2666)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         NoteBook DDR4(3200)

test_Menu_COMPUTER HARDWARE (DIY)_Case
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          AEROCOOL
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          ASUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          ANTEC
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          COOLER MASTER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          CORSAIR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          CUBIC
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          GVIEW
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          ITSONAS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         MARS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         MSI
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         MONTECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         NUBWO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[14]/div/a/label         NZXT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[15]/div/a/label         SilverStone
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[16]/div/a/label         ThermalTake
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[17]/div/a/label         TSUNAMI
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[18]/div/a/label         VIKINGS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[19]/div/a/label         Accessories Case

test_Menu_COMPUTER HARDWARE (DIY)_Power Supply
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          400W.-750W. 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          400W.-750W.FULL
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          (80+White)450W.-550W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          (80+White)600W.-750W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          (80+Bronze)450W.-550W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          (80+Bronze)600W.-700W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          (80+Bronze)750W.-850W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          (80+Silver)500W.-850W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         (80+Gold)550W.-750W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         (80+Gold)800W.-1300W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         (80+Platinum)750W.-1500W.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         (80+Titanium)850W.-1500W.

test_Menu_COMPUTER HARDWARE (DIY)_Cooling System
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          FAN CASE(Other)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          FAN CASE(Hi-End)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          CPU COOLER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          SILICONE CPU
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          LIQUID COOLING

test_Menu_COMPUTER HARDWARE (DIY)_Computer Set DIY 
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[4]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          COMPUTER SET DIY

test_Menu_MONITOR_Monitor 22" - 24.5"
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[5]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Monitor 23'' - 23.6''
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Monitor 23.8'' - 24.5''

test_Menu_MONITOR_Monitor 25" - 29"
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[5]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Monitor 25'' - 29''

test_Menu_MONITOR_Monitor 30" - 39"
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[5]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Monitor 31.5'' - 32''
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Monitor 34'' - 37.5"

test_Menu_MONITOR_Monitor 40'' UP
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[5]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Monitor 43'' - 49''

test_Menu_HARD DISK / STORAGE_Solid State Drive (SSD)
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[6]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          SSD SATA 2.5'' 128 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          SSD SATA 2.5'' 256 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          SSD SATA 2.5'' 512 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          SSD SATA 2.5'' 1 TB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          SSD SATA 2.5'' 2 TB up
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          SSD M.2 SATA 128 GB 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          SSD M.2 SATA 256 GB 
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          SSD M.2 SATA 512 GB up
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         SSD M.2 PCIe/NVMe 256 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         SSD M.2 PCIe/NVMe 512 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         SSD M.2 PCIe/NVMe 1 TB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         SSD M.2 PCIe/NVMe 2 TB up

test_Menu_HARD DISK / STORAGE_Hard Disk for PC
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[6]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Hard Disk CCTV (Surveillance)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Hard Disk PC SATA-III (500 GB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          Hard Disk PC SATA-III (1 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          Hard Disk PC SATA-III (2 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          Hard Disk PC SATA-III (3 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          Hard Disk PC SATA-III (4 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          Hard Disk PC SATA-III (6 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          Hard Disk PC SATA-III (8 TB up)

test_Menu_HARD DISK / STORAGE_Hard Disk for Notebook
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[6]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Hard Disk NoteBook SATA-III

test_Menu_HARD DISK / STORAGE_SSD External
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[6]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          SSD External 256 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          SSD External 512 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          SSD External 1 TB up

test_Menu_HARD DISK / STORAGE_Hard Disk External
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[6]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Hard Disk External 2.5'' (1 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Hard Disk External 2.5'' (2 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          Hard Disk External 2.5'' (4 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          Hard Disk External 2.5'' (5 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          Hard Disk External 3.5'' (2-3 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          Hard Disk External 3.5'' (4 TB)
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          Hard Disk External 3.5'' (6 TB up)

test_Menu_MEMORY / FLASH DRIVE / READER_Flash Drive
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[7]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Flash Drive 16 GB.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Flash Drive 32 GB.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          Flash Drive 64 GB.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          Flash Drive 128 GB.
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          Dual USB OTG Drive
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          Dual USB TYPE-C Drive
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          Flash Drive 256 GB.

test_Menu_MEMORY / FLASH DRIVE / READER_Micro SD Card
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[7]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          MICRO SD CARD 200 - 512 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          MICRO SD CARD 4-8 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          MICRO SD CARD 16 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          MICRO SD CARD 32 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          MICRO SD CARD 64 GB
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          MICRO SD CARD 128 GB

test_Menu_MEMORY / FLASH DRIVE / READER_SD Card
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[7]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          External Card Reader
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          SD CARD

test_Menu_MOUSE / KEYBOARD / PAD_Keyboard
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[8]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          HP
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          LOGITECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          RAPOO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          MICROSOFT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          MD-TECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          NUBWO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          OKER

test_Menu_MOUSE / KEYBOARD / PAD_Keyboard & Mouse
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[8]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          HP
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          LOGITECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          RAPOO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          MICROSOFT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          GENIUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          SMILE
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          MD-TECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          NUBWO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         OKER

test_Menu_MOUSE / KEYBOARD / PAD_Mouse
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[8]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          HP
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          DELL
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          LOGITECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          RAPOO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          MICROSOFT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          GENIUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          SMILE
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          MD-TECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         NUBWO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         OKER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         PEN MOUSE

test_Menu_GAMING ACCESSORIES_Gaming Mouse
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[9]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          FANTECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          NEOLUTION E-SPORT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          NUBWO-X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          SIGNO E-SPORT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          WOLVES-ORIGIN
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          ACER PREDATOR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          ASUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          CORSAIR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         COOLER MASTER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         HYPER-X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         LOGITECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         RAZER   
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[14]/div/a/label         STEELSERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[15]/div/a/label         THUNDER X

test_Menu_GAMING ACCESSORIES_Gaming Keyboard
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[9]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          RAZER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          STEELSERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          CORSAIR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          HYPER-X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          LOGITECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          ASUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          HP
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          COOLER MASTER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         FANTECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         SIGNO E-SPORT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         NUBWO X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         NEOLUTION ESPORT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[14]/div/a/label         REDRAGON
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[15]/div/a/label         OKER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[16]/div/a/label         AULA
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[17]/div/a/label         SIGNO

test_Menu_GAMING ACCESSORIES_Gaming Keyboard & Mouse
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[9]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          FANTECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          SIGNO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          COOLER MASTER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          HP

test_Menu_GAMING ACCESSORIES_Gaming Headset
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[9]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          RAZER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          STEELSERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          CORSAIR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          HYPER-X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          LOGITECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          LENOVO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          ASUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          ACER PREDATOR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         COOLER MASTER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         THUNDER X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         FANTECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         SIGNO E-SPORT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[14]/div/a/label         NUBWO X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[15]/div/a/label         NEOLUTION E-SPORT

test_Menu_GAMING ACCESSORIES_Gaming PAD / แผ่นรองมือ
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[9]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          RAZER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          STEELSERIES
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          CORSAIR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          HYPER-X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          ASUS
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          ACER PREDATOR
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          FANTECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          SIGNO E-SPORT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         NUBWO-X
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         REDRAGON

test_Menu_GAMING ACCESSORIES_Gaming Chair
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[9]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          ANDA-SEAT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          AKRACING
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          MSI
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          COOLER MASTER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          THUNDER X3
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[7]/div/a/label          ONEX
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label          TESORO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[9]/div/a/label          FANTECH
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[10]/div/a/label         SIGNO E-SPORT
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[11]/div/a/label         NUBWO
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[12]/div/a/label         OKER
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[13]/div/a/label         NEOLUTION

test_Menu_GAMING ACCESSORIES_Gaming Mouse Bungee
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[9]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[8]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          SIGNO E-SPORT

test_Menu_ACCESSORIES_Controller (Joy)
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[10]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Controller Wireless/Bluetooth
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Controller XBOX
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          Controller Play2
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          Controller เดี่ยว
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[6]/div/a/label          Controller Racing

test_Menu_SOFTWARE_Antivirus / Internet Security
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[11]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Antivirus
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Total Security

test_Menu_SMARTPHONE & TABLET_Smartphone
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[13]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Smartphone SAMSUNG
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Smartphone HUAWEI
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          Smartphone VIVO

test_Menu_SMARTPHONE & TABLET_Tablet
     Click Element       //*[@id="primary_nav_wrap"]/ul/li[13]/a
     Click Element       //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[2]/div/a/label          Tablet APPLE
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[3]/div/a/label          Tablet SAMSUNG
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[4]/div/a/label          Tablet HUAWEI
     Element Text Should Be     //html/body/section[2]/div[1]/div/div[2]/div[4]/div/div[5]/div/a/label          Tablet LENOVO

     




















#Check product quantity
#    Click Element          //*[@id="show_buy"]          #คลิกเมนู สั่งซื้อสินค้าผ่าน Web
#    Click Element          //*[@id="buy"]/div/span/a          #คลิกเมนู สินค้าคอมพิวเตอร์
#    Go To          https://www.advice.co.th/branch-a001/system_new1/price_ordernew/orderling.php?basket_id=MTQ4ODcx          #GO หน้าเลือกสินค้า
#    Input Text          id=search          ${Product_Code}          #พิมพ์โค๊ดสินค้า
#    Click Element          //*[@id="search_button"]          #คลิกปุ่ม search
#    Input Text          id:qty0          1          #ใส่จำนวนสินค้า
#    Alert Should Not Be Present          timeout=2
#    Click Element          //*[@id="cover_table_price"]/table/tfoot/tr/td/div/a[2]/img          #คลิกปุ่ม เช็คจำนวนสินค้าในคลัง
#    Alert Should Not Be Present          timeout=40
#    Element Text Should Be          //*[@id="cover_table_price"]/table/thead/tr[2]/td[2]/span             สินค้า
    #123456