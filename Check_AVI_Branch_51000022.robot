*** Settings ***
Library          SeleniumLibrary

*** Variables ***
${Browser}          gc
${Link_Web}          https://www.advice.co.th/branch-a022/
${Product_Code}          A0000105
${product quantity}          1
${User_Login}          80006225
${Pass_Login}          uat1234
*** Test Cases ***
Open_Browser
    Open Browser          ${Link_Web}          ${Browser}
    Maximize Browser Window
    Set Selenium Implicit Wait          1

Login_Branch_51000022
    Click Element          //*[@id="top-menu"]/div[1]/ul/li[3]
    Input Text          id:email-signin2          ${User_Login}
    Alert Should Not Be Present          timeout=3
    Input Text          //*[@id="pass-signin2"]          ${Pass_Login}
    Alert Should Not Be Present          timeout=3
    Click Element          //*[@id="signin-form2"]/div[4]/div/button          #คลิกปุ่ม ลงชื่อเข้าใช้
    Alert Should Not Be Present          timeout=2

Check product quantity
    Click Element          //*[@id="show_buy"]          #คลิกเมนู สั่งซื้อสินค้าผ่าน Web
    #Click Element          //*[@id="buy"]/div/span/a          #คลิกเมนู สินค้าคอมพิวเตอร์
    Go To          https://www.advice.co.th/branch-a022/system_new1/price_ordernew/orderling.php?basket_id=Nzc1OA==          #GO หน้าเลือกสินค้า
    Input Text          id=search          ${Product_Code}          #พิมพ์โค๊ดสินค้า
    Click Element          //*[@id="search_button"]          #คลิกปุ่ม search
    Input Text          id:qty0          1          #ใส่จำนวนสินค้า
    Alert Should Not Be Present          timeout=2
    Click Element          //*[@id="cover_table_price"]/table/tfoot/tr/td/div/a[2]/img          #คลิกปุ่ม เช็คจำนวนสินค้าในคลัง
    Alert Should Not Be Present          timeout=30
    Element Text Should Be          //*[@id="cover_table_price"]/table/thead/tr[2]/td[2]/span             สินค้า