*** Settings ***
Library           Selenium2Library

*** Variables ***
${BROWSER}          Chrome  #headlesschrome
${SERVER}          http://commart.advicebkk.com/index.php/branch   #https://www.advice.co.th/search?keyword=A0128421
${ADD_User}          560406   #555555
${ADD_password}          Passwordold975@   #Adv@Pass01
${Customer_code}          90000002
${PRODUCT_SN}          5906589984
${TAX_ID}          4614831116669

*** Test Cases ***
#Test Produce Sale#
Open browsers
    open browser          ${SERVER}          ${BROWSER}
    Maximize Browser Window  # Set Window Size    1920    1080    #Maximize Browser Window
    Set Browser Implicit Wait          1

Login
    Click Element          //*[@id="select-branch"]/div/div[1]   #คลิกโชว์ให้เลือกร้านสาขา
    Set Browser Implicit Wait          1
    Click Element          //*[@id="select-branch"]/div/div[2]/div/div[1]   #คลิกเลือกร้านสาขา
    Set Browser Implicit Wait          1

Add Username
    [Arguments]          ${ADD_User}
    Input Text          id:username          ${ADD_User}
    Set Browser Implicit Wait          1

Add password
    [Arguments]          ${ADD_password}
    Input Text          id:pass          ${ADD_password}
    Set Browser Implicit Wait          1   
Click_login
    Click Element          //html/body/div[1]/div/div[2]/div[2]/div[4]/button         #คลิกปุ่ม เข้าสู่ระบบ
    Set Browser Implicit Wait          1

Selling_Product
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[3]/a     #คลิกเลือกเมนู ขายสินค้า
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[3]/ul/li[2]/a    #คลิกเลือกเมนู โปรแกรมขาย POS
    Set Browser Implicit Wait          3
    Input Text          id:search_cuscode          ${Customer_code}    #กรอก Customer_code
    Set Browser Implicit Wait          1
    Input Text          id:barcode          ${PRODUCT_SN}    #ยิง SN
    Set Browser Implicit Wait          3
    Press Key          id:barcode            \\13            # ASCII code for enter key = \\13   #Enter keyboard
    Input Text          id:custel          0820000000
    Click Element          id:type_paid_01       #คลิกโชว์ ชำระเงินสด
    Click Element          //*[@id="type_paid_01"]/option[2]         #คลิกเลือก ชำระเงินสด
    Input Text          id:amoung_01          539       #กรอกชำนวณเงิน
    Input Text          id:cash_rev          539       #กรอก รับเงิน Cash
    Alert Should Not Be Present          timeout=1
    Click Button          	id:save_btn       #คลิกบันทึก Save
    Alert Should Not Be Present          timeout=1

Confrim_Order_By_Salesperson_Code
    Input Text          id:poss_confirm            000140          #กรอกรหัสพนักงาน ยืนยันการซื้อสินค้า
    Click Element          //html/body/div[2]/div/div/div[3]/button[1]          #คลิกปุ่ม ยืนยัน
    Alert Should Not Be Present          timeout=1
    Click Element          //html/body/div[2]/div/div/div[3]          #คลิกปุ่ม ตกลง รับทราบคำสั่งซื้อสำเร็จ   

Product_Return_To_Status_002 --> 008
    Go To          http://commart.advicebkk.com/index.php/branch/menu        #Go To home
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[5]/a/b          #คลิกเลือกเมนู การเงิน
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[5]/ul/li[6]/a/span          #คลิกเลือกเมนู โปรแกรมรับคืนสินค้าจากลูกค้า
    Input Text          id:sn          ${PRODUCT_SN}    #ยิง SN
    Set Browser Implicit Wait          3
    Press Key          id:sn          \\13                     # ASCII code for enter key = \\13   #Enter keyboard
    Click Element          id:return_percent          # คลิกโชว์หัก % การรับคืนสินค้า
    Click Element          //*[@id="return_percent"]/option[2]          # คลิกเลือกหัก 0% 
    Click Element          id:remark_condition          #  คลิกโชว์ หมายเหตุรับคืนสินค้า 
    Click Element          //*[@id="remark_condition"]/option[2]          #คลิกเลือก สินค้าผิดรุ่น
    Input Text          id:remark          ทดสอบการรับคืน          #กรอกหมายเหตุรับคืนสินค้า 
    Click Element          //*[@id="form"]/div[1]/div[2]/div[2]/div/div[1]/div/button          #คลิกปุ่ม SAVE รับคืนสินค้า
    Input Text          id:return_name          นาย วีระ ทดสอบ
    Input Text          id:return_address          บริษัท แอดไวซ์ ไอที อินฟินิท จำกัด เลขที่ 74/1 หมู่ 1 ถ.ราชพฤกษ์ ต.ท่าอิฐ อ.ปากเกร็ด จ.นนทบุรี 11120
    Input Text          id:return_tax          ${TAX_ID}
    Click Element          //*[@id="form_detail"]/div[4]/div/button[1]          #คลิกปุ่ม SAVE Form ระบุข้อมูลลูกค้าเพื่อออกใบลดหนี้
    Click Element          //html/body/div[7]/div/div/div[3]/button[2]          #คลิกปุ่ม SAVE Form แจ้งเตือนการบันทึก
    Set Browser Implicit Wait          1
    Go To          http://commart.advicebkk.com/index.php/branch/menu        #Go To home
    Alert Should Not Be Present          timeout=1

Product_Return_To_Status_008 --> 001
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[4]/a          #คลิกเลือกเมนู Stock 
    Alert Should Not Be Present          timeout=1
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[4]/ul/li[3]/a/span          #คลิกเลือกเมนู โปรแกรมเช็คสินค้ารับคืนเข้าสต๊อก
    Alert Should Not Be Present          timeout=1
    Input Text          //*[@id="sn"]          ${PRODUCT_SN}    #ยิง SN
    Alert Should Not Be Present          timeout=1
    Press Key          //*[@id="sn"]          \\13                     # ASCII code for enter key = \\13   #Enter keyboard
    Alert Should Not Be Present          timeout=1
    Click Element          //*[@id="form"]/div/div[2]/div[1]/div/div/div/div/button[2]/span          #คลิกปุ่ม บันทึก
    Click Element          //html/body/div[7]/div/div/div[3]/button[2]          #คลิกปุ่ม Yes
    Go To          http://commart.advicebkk.com/index.php/branch/menu        #Go To home

Check_Status_001
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[9]/a/span         #คลิกเลือกเมนู Stock 
    Alert Should Not Be Present          timeout=1
    Input Text          id:barcode          ${PRODUCT_SN}    #ยิง SN
    Alert Should Not Be Present          timeout=1
    Press Key          id:barcode          \\13                     # ASCII code for enter key = \\13   #Enter keyboard
    Alert Should Not Be Present          timeout=1
    Textfield Should Contain          id:store          001

Refund customers
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[5]/a          #คลิกเลือกเมนู การเงิน
    Alert Should Not Be Present          timeout=1
    Click Element          //*[@id="navbar-collapse-1"]/ul[1]/li[5]/ul/li[1]/a          #คลิกเลือกเมนู โปรแกรมบันทึกการชำระเงินจากลูกค้า
    Click Element          //*[@id="main"]/div/div[3]/div[3]/span[1]          #คลิกปุ่ม ค้นหาข้อมูล
    Input Text          id:browse-cuscode          ${Customer_code}          #กรอก Customer_code
    Alert Should Not Be Present          timeout=1
    Press Key          id:browse-cuscode         \\13          # ASCII code for enter key = \\13   #Enter keyboard
    Click Element          //html/body/div[3]/div[2]/div/div/div[2]/div[2]/div/table/tbody/tr/td[1]/a          #คลิกเลือก Customer_code
    Input Text          //*[@id="tbl_payment"]/tbody/tr/td[7]/input          -539          #คีย์ยอด Refund customers
    Press Key          //*[@id="tbl_payment"]/tbody/tr/td[7]/input         \\13          # ASCII code for enter key = \\13   #Enter keyboard 
    Click Element          //*[@id="type_paid_01"]          #คลิก type_paid
    Click Element          //*[@id="type_paid_01"]/option[2]          #คลิก type_paid เงินสด
    Alert Should Not Be Present          timeout=1
    Input Text          id:amoung_01          -539          #คีย์ยอด Refund customers  ที่ช่องรูปแบบการชำระเงิน
    #Textfield Should Contain          id:cash_total          -539         #จ่ายรวม Total
    Click Element          id:save_btn          #คลิกปุ่ม  บันทึก Save
    Alert Should Not Be Present          timeout=1
    Click Element          //html/body/div[9]/div/div/div[3]/button[2]         #คลิกปุ่ม Yes Confrim Save
    Go To          http://commart.advicebkk.com/index.php/branch/menu        #Go To home  
    #Click Element          //html/body/div[9]/div/div/div[3]/button               #คลิกปุ่ม Ok Confrim Save
    #Close Browser
#C:\Users\threerapong.ch\Desktop\WEB_BRANCH_POS_SALE\POS_SALE_NON_PRO.robot  
